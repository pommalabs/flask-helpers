# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from starlite_commons.scripting import git_version as _git_version
from starlite_commons.scripting import start as _start
from starlite_commons.scripting import test as _test

LIB_MODULE = "starlite_commons"
APP_MODULE = "skeleton_app"


def git_version():
    _git_version()


def start():
    _start(APP_MODULE)


def test():
    _test(LIB_MODULE, APP_MODULE)
