# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from pydantic_openapi_schema.v3_1_0 import Components
from starlite import (
    NotAuthorizedException,
    OpenAPIConfig,
    Provide,
    Router,
    Starlite,
    get,
    post,
)
from starlite.status_codes import HTTP_500_INTERNAL_SERVER_ERROR

from skeleton_app.models import WeatherForecast
from skeleton_app.services import WeatherForecastService, get_health_check
from starlite_commons import (
    API_KEY_SECURITY_SCHEME,
    API_KEY_SECURITY_SCHEME_NAME,
    APP_DESCRIPTION,
    APP_VERSION,
    MIT_LICENSE,
    ApiKeyAuthenticationMiddleware,
    HttpTracingMiddleware,
    check_health,
    get_fair_use_token,
    get_version,
    logging_exception_handler,
    on_app_init,
    redirect_to_docs,
)
from starlite_commons.models import MailMessage
from starlite_commons.services import SmtpClient

root = Router(
    path="/",
    route_handlers=[redirect_to_docs, check_health, get_version],
    dependencies={
        "health_check": Provide(get_health_check, use_cache=True),
    },
)


@get(
    "/weather-forecast",
    tags=["weather_forecast"],
    raises=[NotAuthorizedException],
    summary="Returns a random weather forecast.",
)
def get_weather_forecast(
    weather_forecast_svc: WeatherForecastService,
) -> WeatherForecast:
    return weather_forecast_svc.get_forecast()


@post(
    "/send-mail",
    tags=["mail_sender"],
    raises=[NotAuthorizedException],
    summary="Sends a mail message.",
)
def send_mail(
    smtp_client: SmtpClient,
    data: MailMessage,
) -> None:
    with smtp_client.open_connection() as smtp_conn:
        smtp_conn.send_mail(data)


public_api_v1 = Router(
    path="/api/v1",
    route_handlers=[get_fair_use_token],
    middleware=[HttpTracingMiddleware.define()],
    exception_handlers={HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler()},
)

restricted_api_v1 = Router(
    path="/api/v1",
    route_handlers=[get_weather_forecast, send_mail],
    security=[{API_KEY_SECURITY_SCHEME_NAME: []}],
    middleware=[
        ApiKeyAuthenticationMiddleware.define(),
        HttpTracingMiddleware.define(),
    ],
    exception_handlers={HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler()},
    dependencies={
        "weather_forecast_svc": Provide(WeatherForecastService),
    },
)


app = Starlite(
    openapi_config=OpenAPIConfig(
        title="Skeleton App",
        description=APP_DESCRIPTION,
        version=APP_VERSION,
        license=MIT_LICENSE,
        components=Components(
            securitySchemes={API_KEY_SECURITY_SCHEME_NAME: API_KEY_SECURITY_SCHEME}
        ),
    ),
    route_handlers=[root, public_api_v1, restricted_api_v1],
    on_app_init=[on_app_init],
)
