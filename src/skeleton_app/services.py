# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
import random

from healthcheck import HealthCheck

from skeleton_app.models import WeatherForecast


def randomly_failing_check() -> tuple[bool, str]:
    success = random.random() < 0.5  # nosec B311
    return (success, "OK" if success else "KO")


def get_health_check() -> HealthCheck:
    health_check = HealthCheck()
    health_check.add_check(randomly_failing_check)
    return health_check


class WeatherForecastService:
    def __init__(self, logger: logging.Logger):
        self._logger = logger

    def get_forecast(self) -> WeatherForecast:
        self._logger.info("Forecast request received")
        return WeatherForecast(hourly=[])
