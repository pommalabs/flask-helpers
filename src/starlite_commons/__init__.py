# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from opencensus.trace import config_integration

from starlite_commons.constants import (
    API_KEY_HEADER_NAME,
    API_KEY_SECURITY_SCHEME,
    API_KEY_SECURITY_SCHEME_NAME,
    APP_DESCRIPTION,
    APP_VERSION,
    MIT_LICENSE,
)
from starlite_commons.event_handlers import on_app_init
from starlite_commons.middleware import (
    ApiKeyAuthenticationMiddleware,
    HttpTracingMiddleware,
)
from starlite_commons.route_handlers import (
    check_health,
    get_fair_use_token,
    get_version,
    redirect_to_docs,
)
from starlite_commons.services import logging_exception_handler

# According to OpenCensus documentation, following integration only takes effect
# for loggers created __after__ the integration itself. Placing that call
# in this part of the module should make sure that the it is performed before
# first Starlite app logger is created.
config_integration.trace_integrations(["logging"])

__all__ = [
    # Constants
    "API_KEY_HEADER_NAME",
    "API_KEY_SECURITY_SCHEME",
    "API_KEY_SECURITY_SCHEME_NAME",
    "APP_DESCRIPTION",
    "APP_VERSION",
    "MIT_LICENSE",
    # Middleware
    "ApiKeyAuthenticationMiddleware",
    "HttpTracingMiddleware",
    # Route handlers
    "check_health",
    "get_fair_use_token",
    "get_version",
    "redirect_to_docs",
    # Services
    "logging_exception_handler",
    "on_app_init",
]
