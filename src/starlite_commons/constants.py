# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import tomllib

from pydantic import AnyUrl, parse_obj_as
from pydantic_openapi_schema.v3_1_0 import License, SecurityScheme


def _read_pyproject_field(field_name: str) -> str:
    with open("pyproject.toml", mode="rb") as pyproject_file:
        parsed_pyproject = tomllib.load(pyproject_file)
        return str(parsed_pyproject["tool"]["poetry"][field_name])


APP_DESCRIPTION = _read_pyproject_field("description")
APP_VERSION = _read_pyproject_field("version")

API_KEY_HEADER_NAME = "X-Api-Key"
API_KEY_SECURITY_SCHEME_NAME = "api_key"
API_KEY_SECURITY_SCHEME = SecurityScheme(
    type="apiKey",
    name=API_KEY_HEADER_NAME,
    security_scheme_in="header",  # type: ignore
)

MIT_LICENSE = License(
    name="MIT", url=parse_obj_as(AnyUrl, "https://opensource.org/licenses/MIT")
)
