# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import datetime
import sys
from enum import StrEnum
from functools import cache
from ipaddress import IPv4Address, IPv6Address
from uuid import UUID

from pydantic import BaseModel, BaseSettings, Field

from starlite_commons.models import NameEmail


class BaseConfig:
    frozen = True
    if "pytest" not in sys.modules:
        # Environment file should not be read when running under pytest.
        # That should avoid configuration mistakes and undesired external calls.
        env_file = ".env"
        env_file_encoding = "utf-8"


class ApiKey(BaseModel):
    name: str = Field(description="Descriptive API key name", min_length=1)
    value: str = Field(description="API key value", min_length=1)
    expires_at: datetime.datetime | None = Field(
        alias="expiresAt",
        default=None,
        description="Optional API key expiration timestamp.",
    )

    class Config:
        # Model is configured in order to be hashable,
        # which is required to create a frozen set of API keys.
        frozen = True


class SecuritySettings(BaseSettings):
    api_keys: frozenset[ApiKey] = Field(
        default=[],
        description="API keys which could be used to perform security checks."
        " Each API key is described by three properties:"
        " a descriptive name, the key value, an optional expiration timestamp.",
    )
    fair_use_token_key: str | None = Field(
        default=None,
        description="Optional secret key used to encrypt fair use tokens."
        " If not specified, a random one will be generated: however,"
        " specifying a key is mandatory when multiple instances are active,"
        " because each instance needs to be able to decrypt tokens generated"
        " by other instances.",
        min_length=44,
        max_length=44,
    )
    fair_use_token_ttl: int = Field(
        default=3600,
        description="Time-to-live, specified in seconds, of fair use tokens."
        " Default TTL is 3600 seconds.",
        gt=0,
    )
    fair_use_token_cache_size: int = Field(
        default=256,
        description="How many validated fair use tokens should be stored"
        " inside an in-memory cache, which is used to verify that tokens"
        " are not sent again. Default size is 256.",
        gt=0,
    )

    class Config(BaseConfig):
        env_prefix = "security_"

    @staticmethod
    @cache
    def load() -> "SecuritySettings":
        return SecuritySettings()

    def is_security_enabled(self) -> bool:
        return len(self.api_keys) > 0


class SmtpConnectionSecurity(StrEnum):
    NONE = "none"
    SSL = "ssl"
    STARTTLS = "starttls"


class SmtpSettings(BaseSettings):
    server: str | IPv4Address | IPv6Address = Field(
        description="Host name or IP address of SMTP server.",
        min_length=1,
    )
    port: int = Field(
        default=0,
        description="SMTP server port.",
    )
    connection_security: SmtpConnectionSecurity = Field(
        description="If and how each SMTP connection should be secured."
        ' Set to "ssl" in order to open always encrypted connections using SSL.'
        ' Set to "starttls" in order to use opportunistic encryption with STARTTLS.'
        " Explicitly set to None in order to open unencrypted SMTP connections."
    )
    user_name: str | None = Field(
        default=None,
        description="The user name to authenticate with."
        " If not specified, SMTP login will not be performed.",
    )
    password: str | None = Field(
        default=None,
        description="The password for the authentication.",
    )
    default_from_address: NameEmail = Field(
        description='Default "From" address, used when it is not specified'
        ' in a given mailing. It is mandatory to specify a default "From" address'
        ' because it can be usually filled with a common "no-reply" address.'
    )
    default_to_addresses: frozenset[NameEmail] = Field(
        default=[],
        description='Default "To" addresses, added to each mailing in addition'
        " to the ones specified in the mail message.",
    )

    class Config(BaseConfig):
        env_prefix = "smtp_"

    @staticmethod
    @cache
    def load() -> "SmtpSettings":
        return SmtpSettings()  # type: ignore


class AppInsightsSettings(BaseSettings):
    connection_string: str | None = Field(
        default=None,
        description="Connection string provided by Azure Application Insights.",
        min_length=1,
    )
    instrumentationkey: UUID | None = Field(
        default=None,
        description="Instrumentation key provided by Azure Application Insights."
        " Explicitly using instrumentation key has been deprecated by Microsoft."
        " Please use a connection string instead.",
    )

    class Config(BaseConfig):
        env_prefix = "appinsights_"

    @staticmethod
    @cache
    def load() -> "AppInsightsSettings":
        return AppInsightsSettings()

    def has_connection_string(self) -> bool:
        return bool(self.connection_string or self.instrumentationkey)

    def get_connection_string(self) -> str:
        if self.connection_string:
            return self.connection_string
        if self.instrumentationkey:
            return f"InstrumentationKey={self.instrumentationkey}"
        raise ValueError(
            "Neither connection string nor instrumentation key have been provided"
        )
