# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import os
import sys
from subprocess import CalledProcessError, check_call, check_output  # nosec

from starlite_commons.services import FairUseTokenManager

DEFAULT_PORT = "8080"


def cmd_call(command: str) -> None:
    try:
        # Command is used for scripting and build automation, it is not an input
        # received from the user or a command which contains user defined parameters.
        check_call(command, shell=True)  # nosec
    except CalledProcessError as err:
        sys.exit(err.returncode)


def cmd_output(command: str) -> str:
    try:
        # Command is used for scripting and build automation, it is not an input
        # received from the user or a command which contains user defined parameters.
        return check_output(command, shell=True, text=True)  # nosec
    except CalledProcessError as err:
        sys.exit(err.returncode)


def git_version() -> None:
    describe_output = cmd_output("git describe --tags").strip().split("-", maxsplit=2)
    version_number = describe_output[0]
    git_height = describe_output[1] if len(describe_output) > 1 else "0"
    commit_sha = cmd_output("git rev-parse --short=8 HEAD").strip()
    version = f"{version_number}.{git_height}+{commit_sha}"
    cmd_call(f"poetry version {version}")


def start(module: str) -> None:
    port = os.getenv("PORT", DEFAULT_PORT)
    cmd_call(
        f"uvicorn {module}.main:app --port {port} --reload"
        " --no-access-log --no-server-header"
    )


def test(*modules: str) -> None:
    hypothesis_flags = ""
    if os.getenv("CI") is not None:
        hypothesis_flags += "--hypothesis-profile ci"
    coverage_flags = "--cov-report xml:coverage.xml --cov tests " + " ".join(
        [f"--cov {m}" for m in modules]
    )
    cmd_call(
        f"pytest --junitxml test-report.xml {coverage_flags} {hypothesis_flags} tests/"
    )


def generate_fair_use_token_key() -> None:
    print(FairUseTokenManager.generate_token_key())
