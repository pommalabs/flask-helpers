# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from email.utils import parseaddr
from enum import StrEnum
from typing import Any, Callable, Generator

from pydantic import BaseModel, Field, errors
from pydantic.utils import Representation
from pydantic.validators import str_validator


class AppVersion(BaseModel):
    """Application version."""

    version: str = Field(
        description="Application version.",
        min_length=1,
        regex=r"^\d+(\.\d+)*(\+[0-9a-f]{8})?$",
    )

    class Config:
        frozen = True
        schema_extra = {
            "example": {
                "version": "1.2+abcdef12",
            }
        }


class HealthCheckStatus(StrEnum):
    """The outcome of a health check run."""

    SUCCESS = "success"
    FAILURE = "failure"


class HealthCheckResult(BaseModel):
    """The result of an individual health check."""

    checker: str = Field(
        description="Name of checker (name of the function registered as a check)."
    )
    output: str = Field(description="Verbal description of status.")
    passed: bool = Field(description="Boolean value to tell if check passed.")
    timestamp: float = Field(description="A timestamp.", gt=0, allow_inf_nan=False)
    expires: float = Field(
        description="A timestamp to tell when the check will expire."
        " This can be used to cache results.",
        gt=0,
        allow_inf_nan=False,
    )
    response_time: float = Field(
        description="A timestamp to tell when the check was run.",
        gt=0,
        allow_inf_nan=False,
    )

    class Config:
        frozen = True
        schema_extra = {
            "example": {
                "checker": "ping",
                "output": "OK",
                "passed": True,
                "timestamp": 1674228614.0613177,
                "expires": 1674228641.0613177,
                "response_time": 0.414222,
            }
        }


class HealthCheckResponse(BaseModel):
    """A system health check response."""

    hostname: str = Field(description="Name of host server.")
    status: HealthCheckStatus = Field(description="Verbal description of status.")
    timestamp: float = Field(description="A timestamp.", gt=0, allow_inf_nan=False)
    results: list[HealthCheckResult] = Field(
        description="List of results. It should contain at least one.", min_items=1
    )

    class Config:
        frozen = True
        schema_extra = {
            "example": {
                "hostname": "server",
                "status": "success",
                "timestamp": 1674228634.5720627,
                "results": [
                    {
                        "checker": "ping",
                        "output": "OK",
                        "passed": True,
                        "timestamp": 1674228614.0613177,
                        "expires": 1674228641.0613177,
                        "response_time": 0.414222,
                    }
                ],
            }
        }


class FairUseToken(BaseModel):
    """Fair use token, used to control communication on public endpoints."""

    token: str = Field(description="Encrypted fair use token.")

    class Config:
        frozen = True
        schema_extra = {
            "example": {
                "token": "base64_encoded_string",
            }
        }


class NameEmail(Representation):
    __slots__ = "_name", "_email"

    def __init__(self, name: str, email: str):
        self._name = name
        self._email = email

    @property
    def name(self) -> str:
        return self._name

    @property
    def email(self) -> str:
        return self._email

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, NameEmail) and (self._name, self._email) == (
            other._name,
            other._email,
        )

    def __hash__(self) -> int:
        return hash((self._name, self._email))

    def __str__(self) -> str:
        return f"{self._name} <{self._email}>"

    @classmethod
    def __modify_schema__(cls, field_schema: dict[str, Any]) -> None:
        field_schema.update(type="string", format="name-email")

    @classmethod
    def __get_validators__(cls) -> Generator[Callable, None, None]:
        yield cls.validate

    @classmethod
    def validate(cls, value: Any) -> "NameEmail":
        if isinstance(value, cls):
            return value
        value = str_validator(value)
        (name, email) = parseaddr(value)
        if email == "" or "@" not in email:
            raise errors.EmailError()
        if name == "":
            name = email[: email.rfind("@")]
        return cls(name.strip(), email.strip())


class MailMessage(BaseModel):
    """Mail message."""

    subject: str = Field(
        description="Subject.",
        min_length=1,
    )
    from_address: NameEmail | None = Field(
        default=None,
        description='Optional "From" address.',
    )
    to_addresses: frozenset[NameEmail] = Field(
        default=[],
        description='List of "To" addresses.',
    )
    plain_text_content: str | None = Field(
        default=None,
        description="Optional plain text content.",
        min_length=1,
    )
    html_content: str | None = Field(
        default=None,
        description="Optional HTML content.",
        min_length=1,
    )

    class Config:
        frozen = True
        schema_extra = {
            "example": {
                "subject": "Hello World",
                "from_address": "brian@example.com",
                "to_addresses": ["dennis@example.com"],
                "text_content": "Have a nice day.",
            }
        }
