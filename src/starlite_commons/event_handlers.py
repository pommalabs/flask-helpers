# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from typing import Callable

from starlite import Provide, State
from starlite.config import AppConfig

from starlite_commons.services import (
    FairUseTokenManager,
    SmtpClient,
    get_logger,
    get_logging_config,
)
from starlite_commons.settings import SecuritySettings, SmtpSettings

################################################################################
# Init
################################################################################


def on_app_init(app_config: AppConfig) -> AppConfig:
    app_config.logging_config = get_logging_config()
    # Settings
    app_config.dependencies["security_settings"] = Provide(
        SecuritySettings.load, use_cache=True
    )
    app_config.dependencies["smtp_settings"] = Provide(
        SmtpSettings.load, use_cache=True
    )
    # Services
    app_config.dependencies["logger"] = Provide(get_logger, use_cache=True)
    app_config.dependencies["fair_use_token_manager"] = Provide(
        FairUseTokenManager, use_cache=True
    )
    app_config.dependencies["smtp_client"] = Provide(SmtpClient, use_cache=True)
    # Event handlers
    app_config.on_startup.insert(0, warn_if_security_disabled)
    return app_config


################################################################################
# Startup
################################################################################


def warn_if_security_disabled(
    _: State | None = None,
    logger_factory: Callable[..., logging.Logger] = get_logger,
    security_settings: SecuritySettings | None = None,
) -> None:
    security_settings = security_settings or SecuritySettings.load()
    if not security_settings.is_security_enabled():
        logger = logger_factory()
        logger.warning(
            "No API key has been configured,"
            " unauthenticated requests will be accepted"
        )
