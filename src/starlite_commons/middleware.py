# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import hmac
import logging
import random
from datetime import datetime, timezone
from typing import Callable

from opencensus.ext.azure.trace_exporter import AzureExporter
from opencensus.trace.attributes_helper import COMMON_ATTRIBUTES
from opencensus.trace.base_exporter import Exporter
from opencensus.trace.blank_span import BlankSpan
from opencensus.trace.propagation.trace_context_http_header_format import (
    TraceContextPropagator,
)
from opencensus.trace.samplers import Sampler
from opencensus.trace.span import Span, SpanKind
from opencensus.trace.tracer import Tracer
from starlite import (
    AbstractAuthenticationMiddleware,
    AbstractMiddleware,
    ASGIConnection,
    AuthenticationResult,
    DefineMiddleware,
    NotAuthorizedException,
    Request,
    ScopeType,
)
from starlite.types import (
    ASGIApp,
    HTTPResponseStartEvent,
    Message,
    Receive,
    Scope,
    Send,
)

from starlite_commons.constants import API_KEY_HEADER_NAME
from starlite_commons.services import get_exporter, get_logger, get_sampler
from starlite_commons.settings import ApiKey, SecuritySettings


class ApiKeyAuthenticationMiddleware(AbstractAuthenticationMiddleware):
    def __init__(
        self,
        app: ASGIApp,
        logger_factory: Callable[..., logging.Logger],
        security_settings: SecuritySettings,
    ):
        super().__init__(app)

        self._logger_factory = logger_factory
        self._security_settings = security_settings
        self._security_enabled = security_settings.is_security_enabled()

    @staticmethod
    def define(
        logger_factory: Callable[..., logging.Logger] = get_logger,
        security_settings: SecuritySettings | None = None,
    ) -> DefineMiddleware:
        security_settings = security_settings or SecuritySettings.load()
        return DefineMiddleware(
            ApiKeyAuthenticationMiddleware,
            logger_factory=logger_factory,
            security_settings=security_settings,
        )

    async def authenticate_request(
        self, connection: ASGIConnection
    ) -> AuthenticationResult:
        if not self._security_enabled:
            # Security has been disabled. Therefore,
            # no check on API keys has to be performed.
            return AuthenticationResult(user="Anonymous")

        request_api_key = self._get_request_api_key(connection)
        valid_api_keys = self._get_valid_api_keys()
        matched_api_key = self._match_api_key(request_api_key, valid_api_keys)

        logger = self._logger_factory()
        logger.info('API key "%s" validated', matched_api_key.name)

        return AuthenticationResult(user=matched_api_key.name)

    def _get_request_api_key(self, connection: ASGIConnection) -> str:
        if (
            API_KEY_HEADER_NAME not in connection.headers
            or not connection.headers[API_KEY_HEADER_NAME]
        ):
            raise NotAuthorizedException("Missing API key")

        return connection.headers[API_KEY_HEADER_NAME]

    def _get_valid_api_keys(self) -> list[ApiKey]:
        # Expired API keys should be removed from the list, in order to reduce
        # time required for validation.
        valid_api_keys = list(
            filter(
                lambda ak: ak.expires_at is None
                or ak.expires_at >= datetime.now(timezone.utc),
                self._security_settings.api_keys,
            )
        )

        # API keys order is randomized in order to handle cases where many keys
        # have been defined and last keys in the list would always take more time
        # to be validated.
        random.shuffle(valid_api_keys)

        return valid_api_keys

    def _match_api_key(
        self, request_api_key: str, valid_api_keys: list[ApiKey]
    ) -> ApiKey:
        # Step 1: look for an API key with received value.
        matched_api_key = next(
            filter(
                # When validating an API key, timing attacks can be mitigated
                # by avoiding to compare strings with the "==" operator.
                lambda ak: hmac.compare_digest(ak.value, request_api_key),
                valid_api_keys,
            ),
            None,
        )

        # Step 2: make sure that filtered API key has not expired.
        if matched_api_key is None:
            raise NotAuthorizedException("Invalid API key")

        return matched_api_key


class HttpTracingMiddleware(AbstractMiddleware):
    # OpenCensus attributes used to trace HTTP requests.
    HTTP_METHOD = COMMON_ATTRIBUTES["HTTP_METHOD"]
    HTTP_HOST = COMMON_ATTRIBUTES["HTTP_HOST"]
    HTTP_ROUTE = COMMON_ATTRIBUTES["HTTP_ROUTE"]
    HTTP_URL = COMMON_ATTRIBUTES["HTTP_URL"]
    HTTP_STATUS_CODE = COMMON_ATTRIBUTES["HTTP_STATUS_CODE"]

    def __init__(
        self, app: ASGIApp, sampler: Sampler, exporter: Exporter | AzureExporter
    ):
        super().__init__(app, scopes={ScopeType.HTTP})

        self._sampler = sampler
        self._exporter = exporter
        self._propagator = TraceContextPropagator()

    @staticmethod
    def define(
        sampler: Sampler | None = None,
        exporter: Exporter | AzureExporter | None = None,
    ) -> DefineMiddleware:
        sampler = sampler or get_sampler()
        exporter = exporter or get_exporter()
        return DefineMiddleware(
            HttpTracingMiddleware, sampler=sampler, exporter=exporter
        )

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        request: Request = Request(scope)
        span_context = self._propagator.from_headers(request.headers)

        tracer = Tracer(
            span_context=span_context,
            sampler=self._sampler,
            exporter=self._exporter,
            propagator=self._propagator,
        )

        with tracer.span("main") as span:
            span.span_kind = SpanKind.SERVER
            self._add_request_attributes(request, span)

            async def send_wrapper(message: Message) -> None:
                if message["type"] == "http.response.start":
                    self._add_response_attributes(message, span)
                await send(message)

            await self.app(scope, receive, send_wrapper)

    def _add_request_attributes(self, request: Request, span: Span | BlankSpan) -> None:
        span.add_attribute(HttpTracingMiddleware.HTTP_METHOD, request.method)
        span.add_attribute(HttpTracingMiddleware.HTTP_HOST, request.url.hostname)
        span.add_attribute(HttpTracingMiddleware.HTTP_ROUTE, request.url.path)
        span.add_attribute(HttpTracingMiddleware.HTTP_URL, str(request.url))

    def _add_response_attributes(
        self, message: HTTPResponseStartEvent, span: Span | BlankSpan
    ) -> None:
        span.add_attribute(HttpTracingMiddleware.HTTP_STATUS_CODE, message["status"])
