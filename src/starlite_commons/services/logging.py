# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from typing import Callable

from starlite import LoggingConfig, Request, Response
from starlite.utils import create_exception_response

from starlite_commons.settings import AppInsightsSettings


def get_logging_config(
    appinsights_settings: AppInsightsSettings | None = None,
) -> LoggingConfig:
    appinsights_settings = appinsights_settings or AppInsightsSettings.load()
    handlers = {}

    if appinsights_settings.has_connection_string():
        handlers["azure"] = {
            "class": "opencensus.ext.azure.log_exporter.AzureLogHandler",
            "level": "DEBUG",
            "connection_string": appinsights_settings.get_connection_string(),
        }

    return LoggingConfig(
        loggers={
            "app": {
                "level": "INFO",
                "handlers": handlers.keys(),
                "propagate": True,
            },
        },
        handlers=handlers,
    )


def get_logger() -> logging.Logger:
    return logging.getLogger("app")


def logging_exception_handler(
    logger_factory: Callable[..., logging.Logger] = get_logger
) -> Callable[[Request, Exception], Response]:
    def handler(_: Request, exc: Exception) -> Response:
        logger = logger_factory()
        logger.exception("Unhandled exception", exc_info=exc)
        return create_exception_response(exc)

    return handler
