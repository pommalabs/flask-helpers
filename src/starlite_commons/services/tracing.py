# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from typing import Any

from opencensus.ext.azure.trace_exporter import AzureExporter
from opencensus.trace.base_exporter import Exporter
from opencensus.trace.samplers import AlwaysOffSampler, AlwaysOnSampler, Sampler

from starlite_commons.settings import AppInsightsSettings


class DummyExporter(Exporter):
    def emit(self, _: Any) -> None:
        # Dummy exporter does nothing with incoming span data.
        pass

    def export(self, _: Any) -> None:
        # Dummy exporter does nothing with incoming span data.
        pass


def get_sampler(
    appinsights_settings: AppInsightsSettings | None = None,
) -> Sampler:
    appinsights_settings = appinsights_settings or AppInsightsSettings.load()
    if appinsights_settings.has_connection_string():
        return AlwaysOnSampler()
    return AlwaysOffSampler()


def get_exporter(
    appinsights_settings: AppInsightsSettings | None = None,
) -> Exporter | AzureExporter:
    appinsights_settings = appinsights_settings or AppInsightsSettings.load()
    if appinsights_settings.has_connection_string():
        connection_string = appinsights_settings.get_connection_string()
        return AzureExporter(connection_string=connection_string)
    return DummyExporter()
