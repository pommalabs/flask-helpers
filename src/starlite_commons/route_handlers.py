# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from healthcheck import HealthCheck
from starlite import Redirect, Response, get
from starlite.status_codes import HTTP_307_TEMPORARY_REDIRECT

from starlite_commons.constants import APP_VERSION
from starlite_commons.models import AppVersion, FairUseToken, HealthCheckResponse
from starlite_commons.services import FairUseTokenManager

_app_version = AppVersion(version=APP_VERSION)


@get(["/", "/docs"], status_code=HTTP_307_TEMPORARY_REDIRECT, include_in_schema=False)
def redirect_to_docs() -> Redirect:
    return Redirect(path="/schema/swagger")


@get("/health", tags=["health_check"], summary="Checks application health.")
def check_health(health_check: HealthCheck) -> Response[HealthCheckResponse]:
    message, status_code, _ = health_check.run()
    return Response(HealthCheckResponse.parse_raw(message), status_code=status_code)


@get("/version", tags=["version"], summary="Returns application version.")
def get_version() -> AppVersion:
    return _app_version


@get(
    "/fair-use-token",
    tags=["fair_use_token"],
    summary="Generates and returns an encrypted fair use token.",
)
def get_fair_use_token(fair_use_token_manager: FairUseTokenManager) -> FairUseToken:
    return fair_use_token_manager.generate_token()
