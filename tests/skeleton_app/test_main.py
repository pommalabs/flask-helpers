# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from typing import cast

from starlite import DefineMiddleware
from starlite.testing import TestClient

from skeleton_app.main import app, public_api_v1, restricted_api_v1
from starlite_commons.event_handlers import warn_if_security_disabled
from starlite_commons.middleware import ApiKeyAuthenticationMiddleware


def test_that_app_can_be_loaded():
    # Act
    with TestClient(app=app) as client:
        # Assert
        assert client


def test_that_public_api_v1_router_does_not_enforce_authentication():
    # Assert
    assert not any(
        filter(
            lambda m: cast(DefineMiddleware, m).middleware
            is ApiKeyAuthenticationMiddleware,
            public_api_v1.middleware,
        )
    )


def test_that_restricted_api_v1_router_enforces_authentication():
    # Assert
    assert any(
        filter(
            lambda m: cast(DefineMiddleware, m).middleware
            is ApiKeyAuthenticationMiddleware,
            restricted_api_v1.middleware,
        )
    )


def test_that_app_has_expected_startup_handlers():
    # Assert
    assert len(app.on_startup) == 1
    assert warn_if_security_disabled in app.on_startup


def test_that_app_has_expected_shutdown_handlers():
    # Assert
    assert len(app.on_shutdown) == 0
