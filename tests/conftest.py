# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from starlite_commons.testing import register_hypothesis_profiles

register_hypothesis_profiles()
