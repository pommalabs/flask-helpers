# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import string
from datetime import datetime, timedelta
from uuid import UUID

from hypothesis import strategies as st
from starlite import get

from starlite_commons.settings import ApiKey

STUB_APPINSIGHTS_CONNECTION_STRING = f"InstrumentationKey={UUID(int=0)}"
STUB_APPINSIGHTS_INSTRUMENTATION_KEY = UUID(int=0)

RAISE_NAME_ERROR_ENDPOINT = "/raise"

MAX_TEXT_SIZE = 8
MAX_LIST_SIZE = 3


@get(RAISE_NAME_ERROR_ENDPOINT)
def raise_name_error() -> None:
    raise NameError()


def api_key_strategy(min_expiry=datetime.min, max_expiry=datetime.max):
    return st.builds(
        ApiKey,
        name=st.text(alphabet=string.ascii_letters, min_size=1, max_size=MAX_TEXT_SIZE),
        value=st.text(
            alphabet=string.ascii_letters, min_size=1, max_size=MAX_TEXT_SIZE
        ),
        expiresAt=st.one_of(
            st.none(),
            st.datetimes(
                timezones=st.timezones(), min_value=min_expiry, max_value=max_expiry
            ),
        ),
    )


def valid_api_keys_strategy():
    min_expiry = datetime.now() + timedelta(days=1)
    return st.lists(
        api_key_strategy(min_expiry=min_expiry),
        min_size=1,
        max_size=MAX_LIST_SIZE,
    )


def expired_api_keys_strategy():
    max_expiry = datetime.now() - timedelta(days=1)
    return st.lists(
        api_key_strategy(max_expiry=max_expiry).filter(
            lambda ak: ak.expires_at is not None
        ),
        min_size=1,
        max_size=MAX_LIST_SIZE,
    )
