# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from unittest.mock import patch

from hypothesis import given
from hypothesis import strategies as st
from pydantic import ValidationError, parse_obj_as

from starlite_commons.models import MailMessage, NameEmail
from starlite_commons.services import SmtpClient
from starlite_commons.services import smtp_client as smtp_client_module
from starlite_commons.settings import SmtpConnectionSecurity, SmtpSettings
from tests.utils import MAX_LIST_SIZE, MAX_TEXT_SIZE


def _is_valid_email_address(address: str) -> bool:
    try:
        # Not all email addresses generated by Hypothesis seem to be "parsable"
        # by Pydantic. Unparsable addresses get discarded by except clause.
        parse_obj_as(NameEmail, address)
        return True
    except ValidationError:
        return False


def _email_address_strategy():
    """Generates valid email addresses."""
    return st.emails().filter(_is_valid_email_address)


def _smtp_settings_strategy(
    connection_security: SmtpConnectionSecurity = SmtpConnectionSecurity.STARTTLS,
    user_name_strategy: st.SearchStrategy[str | None] | None = None,
    password_strategy: st.SearchStrategy[str | None] | None = None,
):
    return st.builds(
        SmtpSettings,
        server=st.just("localhost"),
        connection_security=st.just(connection_security),
        default_from_address=_email_address_strategy(),
        default_to_addresses=st.frozensets(
            _email_address_strategy(), max_size=MAX_LIST_SIZE
        ),
        user_name=user_name_strategy if user_name_strategy is not None else st.none(),
        password=password_strategy if password_strategy is not None else st.none(),
    )


def _mail_message_strategy(
    from_address_strategy: st.SearchStrategy[str | None] | None = None,
    plain_text_content_strategy: st.SearchStrategy[str | None] | None = None,
    html_content_strategy: st.SearchStrategy[str | None] | None = None,
):
    return st.builds(
        MailMessage,
        subject=st.just("Subject"),
        from_address=from_address_strategy
        if from_address_strategy is not None
        else st.none(),
        to_addresses=st.frozensets(_email_address_strategy(), max_size=MAX_LIST_SIZE),
        plain_text_content=plain_text_content_strategy
        if plain_text_content_strategy is not None
        else st.none(),
        html_content=html_content_strategy
        if html_content_strategy is not None
        else st.none(),
    )


def _text_strategy():
    return st.text(min_size=1, max_size=MAX_TEXT_SIZE)


@given(_smtp_settings_strategy(SmtpConnectionSecurity.SSL))
def test_that_ssl_encrypted_connection_is_closed_on_exit(smtp_settings: SmtpSettings):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with patch.object(smtp_client_module, "SMTP_SSL") as mock:
        # Act
        with smtp_client.open_connection() as smtp_conn:
            assert smtp_conn is not None
    # Assert
    mock.assert_called_once()
    smtp = mock.return_value
    smtp.close.assert_called_once()


@given(_smtp_settings_strategy(SmtpConnectionSecurity.STARTTLS))
def test_that_starttls_encrypted_connection_is_closed_on_exit(
    smtp_settings: SmtpSettings,
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with patch.object(smtp_client_module, "SMTP") as mock:
        # Act
        with smtp_client.open_connection() as smtp_conn:
            assert smtp_conn is not None
    # Assert
    mock.assert_called_once()
    smtp = mock.return_value
    smtp.close.assert_called_once()


@given(_smtp_settings_strategy(SmtpConnectionSecurity.NONE))
def test_that_unencrypted_connection_is_closed_on_exit(smtp_settings: SmtpSettings):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with patch.object(smtp_client_module, "SMTP") as mock:
        # Act
        with smtp_client.open_connection() as smtp_conn:
            assert smtp_conn is not None
    # Assert
    mock.assert_called_once()
    smtp = mock.return_value
    smtp.close.assert_called_once()


@given(_smtp_settings_strategy(SmtpConnectionSecurity.STARTTLS))
def test_that_tls_mode_is_activated_with_starttls_encrypted_connection(
    smtp_settings: SmtpSettings,
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with patch.object(smtp_client_module, "SMTP") as mock:
        # Act
        with smtp_client.open_connection() as smtp_conn:
            assert smtp_conn is not None
    # Assert
    mock.assert_called_once()
    smtp = mock.return_value
    smtp.starttls.assert_called_once()


@given(_smtp_settings_strategy(SmtpConnectionSecurity.STARTTLS))
def test_that_starttls_encrypted_connection_is_closed_when_tls_activation_fails(
    smtp_settings: SmtpSettings,
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with patch.object(smtp_client_module, "SMTP") as mock:
        mock.return_value.starttls.side_effect = ValueError()
        # Act
        try:
            with smtp_client.open_connection():
                assert False  # An error should be raised before arriving here.
        except ValueError:
            pass
    # Assert
    mock.assert_called_once()
    smtp = mock.return_value
    smtp.close.assert_called_once()


@given(
    _smtp_settings_strategy(user_name_strategy=st.none()),
    _mail_message_strategy(),
)
def test_that_login_is_not_performed_when_user_name_is_not_specified(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_called_once()
    smtp = mock.return_value
    smtp.login.assert_not_called()


@given(
    _smtp_settings_strategy(
        user_name_strategy=_text_strategy(), password_strategy=_text_strategy()
    ),
    _mail_message_strategy(),
)
def test_that_login_is_performed_when_user_name_is_specified(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_called_once()
    smtp = mock.return_value
    smtp.login.assert_called_once()


@given(
    _smtp_settings_strategy(
        user_name_strategy=_text_strategy(), password_strategy=_text_strategy()
    ),
    _mail_message_strategy(),
)
def test_that_login_is_performed_once_when_two_messages_are_sent(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_called_once()
    smtp = mock.return_value
    smtp.login.assert_called_once()


@given(
    _smtp_settings_strategy(),
    _mail_message_strategy(from_address_strategy=st.none()),
)
def test_that_default_from_address_is_used_when_from_address_is_not_specified(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP"),
        patch.object(smtp_client_module, "MIMEMultipart") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_called_once()
    msg = mock.return_value
    _, from_addr = next(filter(lambda c: "From" in c.args, msg.mock_calls)).args
    assert from_addr == str(smtp_settings.default_from_address)


@given(
    _smtp_settings_strategy(),
    _mail_message_strategy(from_address_strategy=_email_address_strategy()),
)
def test_that_default_from_address_is_not_used_when_from_address_is_specified(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP"),
        patch.object(smtp_client_module, "MIMEMultipart") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_called_once()
    msg = mock.return_value
    _, from_addr = next(filter(lambda c: "From" in c.args, msg.mock_calls)).args
    assert from_addr == str(mail_message.from_address)


@given(
    _smtp_settings_strategy(),
    _mail_message_strategy(),
)
def test_that_default_to_addresses_are_added_to_mail_message_to_addresses(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP"),
        patch.object(smtp_client_module, "MIMEMultipart") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_called_once()
    msg = mock.return_value
    _, to_addrs = next(filter(lambda c: "To" in c.args, msg.mock_calls)).args
    for addr in smtp_settings.default_to_addresses:
        assert str(addr) in to_addrs
    for addr in mail_message.to_addresses:
        assert str(addr) in to_addrs


@given(
    _smtp_settings_strategy(),
    _mail_message_strategy(plain_text_content_strategy=_text_strategy()),
)
def test_that_plain_text_content_is_added_to_mime_message_when_available(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP"),
        patch.object(smtp_client_module, "MIMEMultipart"),
        patch.object(smtp_client_module, "MIMEText") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_called_once()
    _, subtype = mock.call_args.args
    assert subtype == "plain"


@given(
    _smtp_settings_strategy(),
    _mail_message_strategy(plain_text_content_strategy=st.none()),
)
def test_that_plain_text_content_is_not_added_to_mime_message_when_not_available(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP"),
        patch.object(smtp_client_module, "MIMEMultipart"),
        patch.object(smtp_client_module, "MIMEText") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_not_called()


@given(
    _smtp_settings_strategy(),
    _mail_message_strategy(html_content_strategy=_text_strategy()),
)
def test_that_html_content_is_added_to_mime_message_when_available(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP"),
        patch.object(smtp_client_module, "MIMEMultipart"),
        patch.object(smtp_client_module, "MIMEText") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_called_once()
    _, subtype = mock.call_args.args
    assert subtype == "html"


@given(
    _smtp_settings_strategy(),
    _mail_message_strategy(html_content_strategy=st.none()),
)
def test_that_html_content_is_not_added_to_mime_message_when_not_available(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP"),
        patch.object(smtp_client_module, "MIMEMultipart"),
        patch.object(smtp_client_module, "MIMEText") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_not_called()


@given(
    _smtp_settings_strategy(),
    _mail_message_strategy(),
)
def test_that_mime_message_is_sent(
    smtp_settings: SmtpSettings, mail_message: MailMessage
):
    # Arrange
    smtp_client = SmtpClient(smtp_settings)
    with (
        patch.object(smtp_client_module, "SMTP") as mock,
        smtp_client.open_connection() as smtp_conn,
    ):
        # Act
        smtp_conn.send_mail(mail_message)
    # Assert
    mock.assert_called_once()
    smtp = mock.return_value
    smtp.sendmail.assert_called_once()
