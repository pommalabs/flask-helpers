# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging

import pytest
from starlite import create_test_client
from starlite.status_codes import HTTP_500_INTERNAL_SERVER_ERROR

from starlite_commons.services import get_logging_config, logging_exception_handler
from starlite_commons.settings import AppInsightsSettings
from starlite_commons.testing import capture_logs_at_level
from tests.utils import (
    RAISE_NAME_ERROR_ENDPOINT,
    STUB_APPINSIGHTS_CONNECTION_STRING,
    STUB_APPINSIGHTS_INSTRUMENTATION_KEY,
    raise_name_error,
)

AZURE_LOG_HANDLER = "azure"


def test_that_get_logging_config_adds_azure_handler_when_connection_string_is_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=STUB_APPINSIGHTS_CONNECTION_STRING, instrumentationkey=None
    )
    # Act
    logging_config = get_logging_config(appinsights_settings)
    # Assert
    assert AZURE_LOG_HANDLER in logging_config.handlers
    assert AZURE_LOG_HANDLER in logging_config.loggers["app"]["handlers"]


def test_that_get_logging_config_adds_azure_handler_when_instrumentation_key_is_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=None, instrumentationkey=STUB_APPINSIGHTS_INSTRUMENTATION_KEY
    )
    # Act
    logging_config = get_logging_config(appinsights_settings)
    # Assert
    assert AZURE_LOG_HANDLER in logging_config.handlers
    assert AZURE_LOG_HANDLER in logging_config.loggers["app"]["handlers"]


def test_that_logger_does_not_contain_azure_handler_when_connection_string_is_not_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=None, instrumentationkey=None
    )
    # Act
    logging_config = get_logging_config(appinsights_settings)
    # Assert
    assert AZURE_LOG_HANDLER not in logging_config.handlers
    assert AZURE_LOG_HANDLER not in logging_config.loggers["app"]["handlers"]


def test_that_logging_exception_handler_logs_raised_exception(
    request: pytest.FixtureRequest,
):
    # Arrange
    with (
        capture_logs_at_level(request, logging.ERROR) as caplog,
        create_test_client(
            route_handlers=raise_name_error,
            exception_handlers={
                HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler()
            },
        ) as client,
    ):
        # Act
        response = client.get(RAISE_NAME_ERROR_ENDPOINT)
        # Assert
        assert response.status_code == HTTP_500_INTERNAL_SERVER_ERROR
        assert "NameError" in response.text
        assert len(caplog.records) == 1
        log_record = caplog.records[0]
        assert log_record.exc_info is not None
        assert isinstance(log_record.exc_info[1], NameError)
