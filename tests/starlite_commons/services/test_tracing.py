# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.


from opencensus.ext.azure.trace_exporter import AzureExporter
from opencensus.trace.samplers import AlwaysOffSampler, AlwaysOnSampler

from starlite_commons.services import get_exporter, get_sampler
from starlite_commons.services.tracing import DummyExporter
from starlite_commons.settings import AppInsightsSettings
from tests.utils import (
    STUB_APPINSIGHTS_CONNECTION_STRING,
    STUB_APPINSIGHTS_INSTRUMENTATION_KEY,
)


def test_that_get_sampler_returns_always_on_sampler_when_connection_string_is_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=STUB_APPINSIGHTS_CONNECTION_STRING, instrumentationkey=None
    )
    # Act
    sampler = get_sampler(appinsights_settings)
    # Assert
    assert isinstance(sampler, AlwaysOnSampler)


def test_that_get_exporter_returns_azure_exporter_when_connection_string_is_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=STUB_APPINSIGHTS_CONNECTION_STRING, instrumentationkey=None
    )
    # Act
    exporter = get_exporter(appinsights_settings)
    # Assert
    assert isinstance(exporter, AzureExporter)


def test_that_get_sampler_returns_always_on_sampler_when_instrumentation_key_is_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=None, instrumentationkey=STUB_APPINSIGHTS_INSTRUMENTATION_KEY
    )
    # Act
    sampler = get_sampler(appinsights_settings)
    # Assert
    assert isinstance(sampler, AlwaysOnSampler)


def test_that_get_exporter_returns_azure_exporter_when_instrumentation_key_is_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(
        connection_string=None, instrumentationkey=STUB_APPINSIGHTS_INSTRUMENTATION_KEY
    )
    # Act
    exporter = get_exporter(appinsights_settings)
    # Assert
    assert isinstance(exporter, AzureExporter)


def test_that_get_sampler_returns_always_off_sampler_when_connection_string_is_not_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(connection_string=None)
    # Act
    sampler = get_sampler(appinsights_settings)
    # Assert
    assert isinstance(sampler, AlwaysOffSampler)


def test_that_get_exporter_returns_dummy_exporter_when_connection_string_is_not_provided():
    # Arrange
    appinsights_settings = AppInsightsSettings(connection_string=None)
    # Act
    exporter = get_exporter(appinsights_settings)
    # Assert
    assert isinstance(exporter, DummyExporter)
