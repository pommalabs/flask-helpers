# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import os
import string
from unittest.mock import patch

import pytest
from hypothesis import given
from hypothesis import strategies as st

import starlite_commons.scripting
from starlite_commons.scripting import generate_fair_use_token_key, start
from starlite_commons.scripting import (
    test as _test,  # Avoids that pytest considers it a test and tries to run it.
)
from starlite_commons.services import FairUseTokenManager


def _module_strategy():
    return st.text(alphabet=string.ascii_lowercase, min_size=32)


@given(_module_strategy())
def test_that_start_invokes_uvicorn(module: str):
    # Arrange
    with patch.object(starlite_commons.scripting, "cmd_call") as mock:
        # Act
        start(module)
        # Assert
        mock.assert_called_once()
        command = mock.call_args.args[0]
        assert "uvicorn" in command
        assert module in command


def test_that_test_invokes_pytest():
    # Arrange
    with patch.object(starlite_commons.scripting, "cmd_call") as mock:
        # Act
        _test()
        # Assert
        mock.assert_called_once()
        command = mock.call_args.args[0]
        assert "pytest" in command


@given(st.lists(_module_strategy()))
def test_that_test_configures_specified_modules_for_code_coverage(modules: list[str]):
    # Arrange
    with patch.object(starlite_commons.scripting, "cmd_call") as mock:
        # Act
        _test(*modules)
        # Assert
        mock.assert_called_once()
        command = mock.call_args.args[0]
        for module in modules:
            assert module in command


def test_that_generate_fair_use_token_key_prints_the_key(
    capsys: pytest.CaptureFixture[str],
):
    # Act
    generate_fair_use_token_key()
    # Assert
    output = capsys.readouterr().out
    assert len(output) == (FairUseTokenManager.TOKEN_KEY_LENGTH + len(os.linesep))
