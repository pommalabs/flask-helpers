# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from unittest.mock import MagicMock

import pytest
from healthcheck import HealthCheck
from hypothesis import given
from hypothesis import strategies as st
from pydantic_factories import ModelFactory
from starlite import Provide, create_test_client
from starlite.status_codes import (
    HTTP_200_OK,
    HTTP_307_TEMPORARY_REDIRECT,
    HTTP_503_SERVICE_UNAVAILABLE,
)

from starlite_commons import APP_VERSION, check_health, get_version, redirect_to_docs
from starlite_commons.models import HealthCheckResponse
from starlite_commons.route_handlers import get_fair_use_token
from starlite_commons.services import FairUseTokenManager

################################################################################
# Route handlers
################################################################################


@pytest.mark.parametrize("endpoint", ["/", "/docs"])
def test_that_root_and_docs_endpoints_redirect_to_openapi_docs(endpoint):
    # Arrange
    with create_test_client(route_handlers=redirect_to_docs) as client:
        # Act
        response = client.get(endpoint, follow_redirects=False)
        # Assert
        assert response.status_code == HTTP_307_TEMPORARY_REDIRECT
        assert response.has_redirect_location
        assert response.headers["location"].endswith("/schema/swagger")


class HealthCheckResponseFactory(ModelFactory[HealthCheckResponse]):
    __model__ = HealthCheckResponse


@given(
    st.builds(HealthCheckResponseFactory.build),
    st.sampled_from([HTTP_200_OK, HTTP_503_SERVICE_UNAVAILABLE]),
)
def test_that_check_health_endpoint_triggers_health_checks(
    result: HealthCheckResponse, status_code: int
):
    # Arrange
    health_check = MagicMock(spec_set=HealthCheck)
    health_check.run = MagicMock(return_value=(result.json(), status_code, None))
    with create_test_client(
        route_handlers=[check_health],
        dependencies={"health_check": Provide(lambda: health_check)},
    ) as client:
        # Act
        response = client.get("/health")
        # Assert
        assert response.status_code == status_code
        assert response.json() == result.dict()


def test_that_get_version_endpoint_responds_with_app_version():
    # Arrange
    with create_test_client(route_handlers=get_version) as client:
        # Act
        response = client.get("/version")
        # Assert
        assert response.status_code == HTTP_200_OK
        assert response.json()["version"] == APP_VERSION


def test_that_get_fair_use_token_responds_with_fair_use_token():
    # Arrange
    with create_test_client(
        route_handlers=get_fair_use_token,
        dependencies={"fair_use_token_manager": Provide(FairUseTokenManager)},
    ) as client:
        # Act
        response = client.get("/fair-use-token")
        # Assert
        assert response.status_code == HTTP_200_OK
        assert len(response.json()["token"]) > 0
