# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import pytest
from pydantic import BaseModel, ValidationError

from starlite_commons.models import NameEmail


def test_that_name_email_parses_valid_data():
    class Model(BaseModel):
        v: NameEmail

    # Arrange
    name1 = "foo bar"
    email1 = "foobaR@example.com"
    name_email1 = NameEmail(name1, email1)
    name2 = "hot bar"
    email2 = "different@example.com"
    name_email2 = NameEmail(name2, email2)
    # Act & Assert
    assert str(Model(v=NameEmail(name1, email1)).v) == str(name_email1)
    assert str(Model(v=f"{name1}  <{email1}>").v) == str(name_email1)  # type: ignore
    assert str(Model(v=f"{email1}").v) == f"foobaR <{email1}>"  # type: ignore
    assert name_email1 == NameEmail(name1, email1)
    assert name_email1 != NameEmail(name1, email2)
    assert name_email1 != NameEmail(name2, email1)
    assert name_email1 != name_email2
    assert hash(name_email1) == hash(NameEmail(name1, email1))
    assert hash(name_email1) != hash(NameEmail(name1, email2))
    assert hash(name_email1) != hash(NameEmail(name2, email1))
    assert hash(name_email1) != hash(name_email2)
    assert name_email1.name == name1
    assert name_email1.email == email1


@pytest.mark.parametrize(
    "name_email",
    ["[invalid!email]", "John <[invalid!email]>", "Jack <invalid-email>"],
)
def test_that_name_email_rejects_invalid_data(name_email: str):
    class Model(BaseModel):
        v: NameEmail

    with pytest.raises(ValidationError):
        Model(v=name_email)  # type: ignore
